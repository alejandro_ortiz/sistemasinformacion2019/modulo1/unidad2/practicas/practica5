﻿DROP DATABASE IF EXISTS practica5;
CREATE DATABASE practica5;
USE practica5;

  -- creacion de tablas
  -- Tabla FEDERACION
  CREATE OR REPLACE TABLE federacion(
    nombre varchar(50),
    direccion varchar (50),
    telefono int,
    PRIMARY KEY (nombre)
  );
  
  -- Tabla MIEMBRO
  CREATE OR REPLACE TABLE miembro(
    dni varchar(9),
    nombre_m varchar(50),
    titulacion varchar(50),
    PRIMARY KEY (dni)
  );

  -- Tabla COMPOSICION
  CREATE OR REPLACE TABLE composicion(
    nombre varchar(50),
    dni varchar(9),
    cargo varchar(50),
    fecha_inicio date,
    PRIMARY KEY (nombre, dni)
  );

  -- Foreign Key
  ALTER TABLE composicion 
    ADD CONSTRAINT fkComposicionFederacion FOREIGN KEY (nombre) REFERENCES federacion(nombre), 
    ADD CONSTRAINT fkComposicionMiembro FOREIGN KEY (dni) REFERENCES miembro(dni);